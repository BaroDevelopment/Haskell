import Prelude hiding (sum, foldr, filter, foldl, (.), ($), flip)

import Data.Char

infixr 5 #
(#) = (-)

square x = x*x

dx = 0.0001

ableitung :: (Float -> Float) -> Float -> Float
ableitung f x = (f (x+dx) - f x) / dx

bla :: (Int -> Int) -> (Int -> Int)
bla f = g
  where g x = f x + 1

blub :: (Int -> Int) -> Int -> Int
blub f x = f x + 1

add1 :: Int -> (Int -> Int)
add1 x y = x + y

add2 :: Int -> Int -> Int
add2 x = \ y -> x + y

add3 :: Int -> Int -> Int
add3 = \ x y -> x + y


plus42 :: [Int] -> [Int]
plus42 = map (+42)

code :: Int -> String -> String
code offSet = map (codeChar offSet)

codeChar :: Int -> Char -> Char
codeChar offSet c = chr ((ord c + offSet) `mod` 256)

--map :: (a -> b) -> [a] -> [b]
--map _ []     = []
--map f (x:xs) = f x : map f xs

map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x -> (f x:)) []

sum :: [Int] -> Int
sum [] = 0
sum (x:xs) = x + sum xs

sum' :: [Int] -> Int
sum' = foldr (+) 0

checkSum :: [Char] -> Int
checkSum "" = 1
checkSum (c:cs) = (ord c + checkSum cs) `mod` 256 

checkSum' :: String -> Int
checkSum' = foldr (\c res -> (ord c + res) `mod` 256) 1

foldr :: (a -> b -> b) -> b -> [a] -> b 
foldr _ e []     = e
foldr f e (x:xs) = f x (foldr f e xs)

filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter p (x:xs) | p x       = x : filter p xs
                | otherwise = filter p xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' p = foldr (\x ys -> if p x then x:ys else ys) []


rev :: [a] -> [a]
rev = foldr (\x ys -> ys ++ [x]) []

rev' :: [a] -> [a]
rev' = foldl (\acc x -> x : acc) [] 
  where
    revH [] acc = acc
    revH (x:xs) acc = revH xs (x:acc)

foldl :: (b -> a -> b) -> b -> [a] -> b
foldl _ e []     = e
foldl f e (x:xs) = foldl f (f e x) xs
 
while :: (a -> Bool) -> (a -> a) -> a -> a
while p f x | p x       = while p f (f x)
            | otherwise = x

--Nachteil : perfomance ist schlecht
--Vorteil: Keine Beschränkung so wie bei Java
--type Array a = Int -> a
data Array a = A (Int -> a)

emptyArray :: Array a
--emptyArray i = error ("Access to non-initialized component " ++ show i)
emptyArray = A (\i -> error ("Access to non-initialized component " ++ show i))


getIndex :: Array a -> Int -> a   --atIndex Funktion
--getIndex a i = a i
getIndex (A a) i = a i

putIndex :: Array a -> Int -> a -> Array a
--putIndex a i v = a' where a' j | i == j = v | otherwise = a j
--putIndex a i v = \j -> if j == i then v else a j
putIndex (A a) i v = A (\j -> if j == i then v else a j)

listToArray :: [a] -> Array a
listToArray xs = listToArray' xs 0
  where listToArray' :: [a] -> Int -> Array a
        listToArray' [] _ = emptyArray
        listToArray' (x:xs) i = putIndex (listToArray' xs (i+1)) i x

--Konsole:  Array definieren          let a = listToArray [1..10]

(.) :: (b -> c) -> (a -> b) -> a -> c
(f . g) x = f (g x)

-- map (((+1) . (*2))) [1..10]
--map (char . (+1) . ord) "Hallo"
-- :i (+)    :i(*)

curry :: ((a, b) -> c) -> a -> b -> c
curry f x y = f (x, y)

uncurry :: (a -> b -> c) -> (a, b) -> c
uncurry f (x, y) = f x y

-- (curry . uncurry) (+) 3 4
-- map (uncurry (+)) (zip [1..10] [40..50])    <==>  map (uncurry (+)) $ zip [1..10] [40..50]

($) :: (a -> b) -> a -> b
-- ($) = id
f $ x = f x
-- (3*) $ 5 + 6

const :: a -> b -> a
const x _ = x
-- map (const 73) [1..10]

andmap :: (a -> Bool) -> [a] -> Bool
andmap _ [] = True
andmap f (x:xs) = f x && andmap f xs

ormap :: (a -> Bool) -> [a] -> Bool
ormap _ [] = False
ormap f (x:xs) = f x || andmap f xs

flip:: (a -> b -> c) -> b -> a -> c
flip f x y = f y x

class MyEq a where
	(===) , (/==) :: a -> a -> Bool

	(/==) = (not .) . (===)   -- <==>  x /=== y = not (x === y)
	x === y = not (x /== y)


instance MyEq Bool where
   True /== False = True
   False /== True = False
   _  === _ = False

data Color = Blue | Red | Yellow

instance MyEq Color where
   Blue === Blue = True
   Red  === Red  = True
   Yellow  === Yellow  = True
   _  === _ = False


instance MyEq a => MyEq [a] where 
	[] === [] = True
	(x:xs) === (y:ys) = x === y && xs === ys
	_ === _ = False

instance (MyEq a, MyEq b) => MyEq (a,b) where
	(x,y) === (x',y') = x === x' && y === y'