import Prelude hiding (some, Maybe(..), Nothing, Just, last, concat, (!!), Right, allJusts, Left, Either, add, zip, unzip, unzip3, lines, take)

--length :: [Int] -> Int
--length [] = 0
--length [_ : xs] = 1 + length xs

-- Optional in Java
-- Maybe ist ein Typkonstruktor, kann man daran erkennen, dass er einen Argument hat
-- a steht für einen beliebigen Typen
data Maybe a = Nothing | Just a

isNothing :: Maybe a -> Bool
isNothing Nothing = True
isNothing (Just _ ) = False

head :: [a] -> a
head (x:_) = x

tail :: [a] -> [a]
tail (_ : xs) = xs

last :: [a] -> a
last [x] = x
last (_ : xs@(_:_)) = last xs

concat :: [[a]] -> [a]
concat [] = []
concat (xs : xss) = xs ++ concat xss

--Gib n-te Element aus Liste
(!!) :: [a] -> Int -> a
(x : _) !! 0 = x
(_ : xs) !! n = xs !! (n - 1)

data Either a b = Left a | Right b

valOrLength :: Either Int String -> Int
valOrLength (Left n) = n
valOrLength (Right str) = length str

--Besser ist aber: add: Int -> Int -> Int
add :: (Int, Int) -> Int
add (x, y) = x + y

first :: (a, b) -> a
first (x, _) = x

second :: (a, b) -> b
second (_ , x) = x

zip :: [a] -> [b] -> [(a,b)]
zip [] _ = []
zip _ [] = []
zip (x:xs) (y:ys) = (x,y) : zip xs ys

unzip :: [(a, b)] -> ([a] , [b])
unzip [] = ([], [])
--Diese Progrmmiertechnik merken ! wird gerne in Klausur abgefragt
unzip((x, y) : xys) = (x:first (unzip xys) , y:second (unzip xys))

unzip2 :: [(a, b)] -> ([a] , [b])
unzip2 [] = ([], [])
unzip2((x, y) : xys) = let (xs, ys) = unzip2 xys
                                        in (x:xs, y:ys)

--case kann mehr als let weil man mehrer Fälle machen kann.
unzip3 :: [(a, b)] -> ([a] , [b])
unzip3 [] = ([], [])
unzip3((x, y) : xys) = case unzip3 xys of (xs, ys) -> (x:xs, y:ys)

allJusts :: [Maybe a] -> [a]
allJusts [] = []
allJusts (mx : mxs) = case mx of 
	                   Nothing -> allJusts mxs
	                   Just y ->  y : allJusts mxs

--Nicht gut !
--lines :: String -> [String]
--lines "" = []
--lines ('\n':cs) = "" : lines cs
--lines (c : cs) = let (str : strs) = lines cs in
--                   (c : str) : strs

--besser
lines :: String -> [String]
lines "" = []
lines ('\n':cs) = "" : lines cs
lines (c : cs) = case lines cs of 
	                [] -> [[c]]
	                (str : strs) -> (c:str):strs



-- let <==> case
-- let p = e1 in e2    <==>  case e1 of p->e2

take :: Int -> [a] -> [a]
take 0 _ = []
take n [] = []
take n (x:xs) = x : take (n - 1) xs



-- ################# Funktionen höherer Ordnung  #######################
dx = 0.001

--(ableitung sin) 0
-- (ableitung square) 40

-- auch möglich  ableitung :: (Float -> Float) -> Float -> Float
ableitung :: (Float -> Float) -> (Float -> Float)
ableitung f = f'
  where f' x = (f (x+dx) - f x) / dx

--Diese Typen sind exakt identisch :
-- (Int -> Int) -> (Int -> Int)    <==>  (Int -> Int) -> Int -> Int

-- Es Gilt (blub (\x -> x + 42)) 30  <===>  blub (\x -> x + 42) 30    wegen Linksassoziativ

bla :: (Int -> Int) -> (Int -> Int)
bla f = g
 where g x = f x + 1

blub :: (Int -> Int) -> Int -> Int
blub f x = f x + 1

plus42 :: [Int] -> [Int]
plus42 [] = []
plus42 (x:xs) = x + 42 : plus42 (xs)

plus42 = map (+ 42)  -- <==> plus42 xs = map (+ 42) xs

code :: Int String -> String
code _ "" = ""
--code offSet (c:cs)  = codeChar offSet c : code offSet cs
code offSet = map (codeChar offSet)

codeChar :: Int -> Char -> Char
codeChar offSet c = ((ord c + offSet) `mod` 256)

map :: (a -> b) -> [a] -> [b]
map _ [] = []
map f (x:xs) = f x : map f xs


sum :: [Int] -> Int
sum [] = 0
sum (x:xs) = x + sum xs

checkSum :: String -> Int
checkSum "" = 1
checkSum (c:cs) = (ord c + checkSum cs) `mod` 256