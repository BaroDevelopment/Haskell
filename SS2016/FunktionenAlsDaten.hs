type Array a = Int -> a

emptyArray :: Array a
emptyArray i = 
	error ("Access to non-initialized component " ++ show i)

getIndex :: Array a -> Int -> a
getIndex a i = a i

putIndex :: Array a -> Int -> a -> Array a
putIndex a i v = a’
   where a’ j | i == j    = v
              | otherwise = a j




type Set a = a -> Bool

empty :: Set a
empty _ = False

insert :: Eq a => a -> Set a -> Set a
insert x s y
            |x == y     = True
            | otherwise = s y

remove :: Eq a => a -> Set a -> Set a
remove x m = difference m (x ==)

isElem :: a -> Set a -> Bool
isElem x s = s x

union :: Set a -> Set a -> Set a
union = lift (||)

intersection :: Set a -> Set a -> Set a
intersection = lift (&&)

lift :: (a -> b -> c) -> (d -> a) 
         -> (d -> b) -> d -> c
lift op f g x = op (f x) (g x)

listToSet :: Eq a => [a] -> Set a
listToSet xs x = x `elem` xs


