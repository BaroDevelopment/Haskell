import Prelude hiding (min, (++), tail, head, last, concat, Maybe(..),
                       (!!), length, zip, unzip, lines, take)
--data Complex
  -- myComplexNumer
  --addComplex

--heightTree
--data Maybe
  --isNothing

--data Either
--valOrLength
-- type boolOrSomething
--add
--allJusts
--lines
--takes

fac :: Int -> Int
fac 0 = 1
fac n = n * fac (n-1)

fibNaive :: Int -> Int
fibNaive 0 = 0
fibNaive 1 = 1
fibNaive n = fib(n-1) + fib(n-2)

fib :: Int -> Int
fib n = fibAcc' 0 1 n
   where fibAcc' fibn fibnp1 n =
          if n == 0
          then fibn
          else fibAcc' fibnp1 (fibn + fibnp1) (n - 1)

fib2 :: Integer -> Integer
fib2 n = fib2' 0 1 n
  where
    fib2' :: Integer -> Integer -> Integer -> Integer
    fib2' fibn fibnp1 n =
      if n==0
        then fibn
        else fib2' fibnp1 (fibn+fibnp1) (dec n)
    dec n = n - 1


--TODO: Funktioniert noch nicht
--fib3 :: Int -> Int
-- fib3 n =
--  let fib3' fibn fibnp1 n =
--        if n == 0 then fibn
--        else fib3' fibnp1 (fibn + fibnp1) (n - 1)


--TODO : Noch nicht nachvollzogen
isPrim :: Int -> Bool
isPrim n = n/=1 && checkDiv (div n 2)
  where checkDiv :: Int -> Bool
        checkDiv m =
            m == 1 || mod n m /=0 && checkDiv (m - 1)

head :: [a] -> a
head (x:_) = x

tail :: [a] -> [a]
tail (_:xs) = xs

lengthList :: [a] -> Int
lengthList [] = 0
lengthList (_:xs) = 1 + lengthList xs

(++) :: [a] -> [a] -> [a]
xs ++ [] = xs                       --  (++) xs [] = xs
[] ++ xs = xs                       --  (++) [] xs = xs
(x:xs) ++ ys = x : (xs ++ ys)       --  (++) (x:xs) ys = x : ((++) xs ys)

concat :: [[a]] -> [a]
concat [] = []
concat (xs:xss) = xs ++ (concat xss)

--Takes a list and a Element and gives the index of the Element
getIndexByElem :: [Int] -> Int -> Int
getIndexByElem (x:xs) n = getIndexByElem' (x:xs) n 0
   where getIndexByElem' (x:xs) n res =
                            if xs == [] then error "Elem not in List"
                            else if x == n then res
                            else getIndexByElem' xs n (res+1)

--takes a list and a Index and gives the Element of the index
getElemByIndex :: [a] -> Int -> a                   --  (!!) :: [a] -> Int -> a
getElemByIndex (x: _) 0 = x                         --  (x:_)  !! 0 = x
getElemByIndex (_:xs) n = getElemByIndex xs (n-1)   --  (_:xs) !! n = xs !! (n-1)

getLastElem :: [a] -> a
getLastElem (x:[]) = x
getLastElem (x:xs) = getLastElem xs

zip :: [a] -> [b] -> [(a,b)]
zip [] _ = []
zip _ [] = []
zip (x:xs) (y:ys) = (x,y) : zip xs ys

--TODO verstehe ich noch nicht
unzip :: [(a,b)] -> ([a], [b])
unzip [] = ([], [])
unzip ((x,y):someList) = let (xs,ys) = unzip someList in
                                       (x:xs , y:ys)
--nikitas version
unzip2 :: [(a,b)] -> ([a], [b])
unzip2 [] = ([] , [])
unzip2 ((ab) : abs) = (a: as ,b: bs)
    where (as, bs) = unzip abs

data Tree a = Empty | Node a (Tree a) (Tree a) deriving Show

treeToList :: Tree a -> [a]
treeToList Empty = []
--treeToList (Node a left right) = (a : treeToList left) ++ (treeToList right)
treeToList2 (Node x tl tr) = treeToList2 tl ++ [x] ++ treeToList2 tr

first :: (a,b) -> a
first (a, _ ) = a

second :: (a,b) -> b
second (_ , b) = b


-- ### Testing Area ###
myList :: [Int]
myList = [1,2,3,4]
myList3 = [1,2,3,4,5]
myList2 = [[1,2,3,4], [5,6,7,8]]

emptyList :: [Int]
emptyList = []

zippy = [(1,1),(2,2),(3,3),(4,4)]

t1 = Node 4 (Node 1 Empty (Node 3 Empty Empty)) (Node 9 (Node 5 Empty Empty) (Node 12 Empty Empty))
