








main :: IO ()
main = do
  putStr "Hallo"
  putStrLn " liebe Studies,"
  putStrLn "bald habt ihr Haskell überstanden."

bla = let blub = replicate 100 (putStrLn "Holla") in
  do
    putStr "Zahl: "
    str <- getLine
    case reads str of
      [(n,"")] -> print (fac n)
      _        -> do
          putStrLn "Das ist keine Zahl du Depp!"
          bla

blub =
  putStr "Zahl: " >>
  getLine >>= \ str ->
  case reads str of
      [(n,"")] -> print (fac n)
      _        -> 
          putStrLn "Das ist keine Zahl du Depp!" >>
          blub

fac :: Integer -> Integer
fac 0 = 1
fac n = n * fac (n-1)

getLn :: IO String
getLn = do
  c <- getChar
  case c of
    '\n' -> return ""
    _    -> do
         cs <- getLn
         return (c:cs)

getLn' :: IO String
getLn' = 
  getChar >>= \c ->
  case c of
    '\n' -> return ""
    _    -> getLn' >>= \cs ->
            return (c:cs)

-- Impementierung von (>>)
(>>>) :: IO a -> IO b -> IO b
a1 >>> a2 = a1 >>= const a2








