ggT :: Int -> Int -> Int
ggT a 0 = a
ggT a b = ggT b (mod a b)

ggTL :: [Int] -> Int
ggTL [x]    = x
ggTL (x:xs) = ggT x (ggTL xs)


kgV :: Int -> Int -> Int
kgV _ 0 = 0
kgV 0 _ = 0
kgV a b = (a * b) `div` ggT a b



kgVL :: [Int] -> Int
kgVL [x]    = x
kgVL (x : xs) = kgV x (kgVL xs)