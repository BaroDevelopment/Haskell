import Prelude hiding (Maybe(..), Either(..))

{-
class Eq a where
  (==), (/=) :: a -> a -> Bool

data IntTree = Empty | Node IntTree Int IntTree

instance Eq IntTree where
  Empty == Empty = True
  Node tl n tr == Node tl’ n’ tr’ = tl == tl’ && n == n’ && tr == tr’
  _ == _ = False
  t1 /= t2 = not (t1 == t2)


-- Polymorphe Version von oben
data Tree a = Empty | Node (Tree a) a (Tree a)
instance Eq a => Eq (Tree a) where
...<wie oben>...



class Eq a where
  (==), (/=) :: a ! a ! Bool
  x1 == x2 = not (x1 /= x2)
  x1 /= x2 = not (x1 == x2)




-}

-- ########## Maybe  ##########
data Maybe a = Nothing | Just a

isNothing :: (Maybe a) -> Bool
isNothing Nothing = True
isNothing (Just _) = False

-- ########## Either  ##########
     -- ermöglicht Liste mit unterschiedlichen Typen
data Either a b = Left a | Right b

diffList = [Left 42, Right "HALLO"] :: [Either Int String]

-- ########## Complex  ##########
data Complex = Complex Float Float
   deriving Show

myComplexNumber :: Complex
myComplexNumber = Complex 3.0 4.0

addComplex :: Complex -> Complex -> Complex
addComplex (Complex r1 i1) (Complex r2 i2) = Complex (r1 + r2) (i1 + i2)

-- ########## Integer List  ##########
data List = Nil | Cons Int List

-- data [Int] = [] | Int:[Int] -- vordefiniert in Haskell

append :: List -> List -> List
append Nil xs = xs
append (Cons x xs) ys = Cons x (append xs ys)

-- ########## Tree - only leafs  ##########
                     -- Nur Blätter sind beschriftet
data Tree2 a = Leaf a
            | Branch2 (Tree2 a) (Tree2 a)
  deriving (Eq, Show)

sumTree :: Tree2 Int -> Int
sumTree (Leaf a) = a
sumTree (Branch2 left right) = sumTree left + sumTree right

mirrorTree :: Tree2 a -> Tree2 a
mirrorTree (Leaf a) = (Leaf a)
mirrorTree (Branch2 left right) = Branch2 (mirrorTree right) (mirrorTree left)

-- schnellere Möglichkeit mit Akkumulatortechnik
sumTree' :: Tree2 Int -> Int
sumTree' = sumTree 0
   where sumTree k (Leaf n) = n + k
         sumTree k (Branch2 left right) = sumTree (sumTree k left) right

toList :: Tree2 a -> [a]
toList (Leaf     x) = [x]
toList (Branch2 l r) = toList l ++ toList r

-- ########## Tree - normal ##########
data Tree a = Empty
            | Branch (Tree a) a (Tree a) 
  deriving (Eq, Show)

-- converts a Tree to a List
treeToList :: Tree a -> [a]
treeToList Empty = []
treeToList (Branch tl x tr) = treeToList tl ++ [x] ++ treeToList tr

height :: (Tree a) -> Int
height Empty = 0
height (Branch tl _ tr) = 1 + max (height tl) (height tr)



-- ########## arithmetischen Ausdrücke ##########
{- Scheme Style
data Expr
  = Const Int
  | Add Expr Expr
  | Sub Expr Expr
  | Mul Expr Expr
  | Div Expr Expr
  deriving Show
-}

data Exp =  Number Int
          | Exp :+: Exp
          | Exp :-: Exp
          | Exp :*: Exp
          | Exp :/: Exp
          deriving Show

test :: Exp
test = (Number 3 :+: Number 4) :*: ((Number 73 :-: Number 37) :/: Number 6)

infixl 6 :+:, :-:
infixl 7 :*:, :/:

seven :: Exp
seven = Number 1 :+: Number 2 :*: Number 3

eval :: Exp -> Int
eval (Number  i) = i
eval (e1 :+: e2) = eval e1 + eval e2
eval (e1 :-: e2) = eval e1 - eval e2
eval (e1 :*: e2) = eval e1 * eval e2
eval (e1 :/: e2) = eval e1 `div` eval e2

-- ########## arithmetischen Ausdrücke als Baum ##########
data ExpTree a b = ExpLeaf a
  | ExpNode b (ExpTree a b) (ExpTree a b)
  deriving Show

data Op = Add | Sub | Mul | Div 
  deriving Show

type Exp' = ExpTree Int Op

-- Damit wir konkrete Ausdrücke einfacher konstruieren können, definieren wir uns 
-- die folgenden Hilfs(konstruktor)funktionen, auch Smartkonstruktoren genannt:
number :: Int -> Exp'
number = ExpLeaf

(.+.) :: Exp' -> Exp' -> Exp'
(.+.) e f = ExpNode Add e f

(.-.) :: Exp' -> Exp' -> Exp'
(.-.) = ExpNode Sub

(.*.) :: Exp' -> Exp' -> Exp'
(.*.) = ExpNode Mul

(./.) :: Exp' -> Exp' -> Exp'
(./.) = ExpNode Div

--Somit könnte unser Ausdruck test von oben wie folgt in der Baumstruktur definiert werden:
test' :: Exp'
test' = (number 3 .+. number 4) .*. ((number 73 .-. number 37) ./. number 6)

{-
Die Auswertungsfunktion ergibt sich wie oben, allerdings müssen wir noch einem Wert des 
Typs Op die dazugehörige Funktion als Bedeutung (Semantik) zuordnen.
-}
sem :: Op -> Int -> Int -> Int
sem Add = (+)
sem Sub = (-)
sem Mul = (*)
sem Div = div

-- Nun können wir die Auswertung wie folgt definieren:
eval' :: Exp' -> Int
eval' (ExpLeaf      n) = n
eval' (ExpNode op l r) = sem op (eval' l) (eval' r)

{-
Üblicherweise möchte man Ausdrücke aber nicht nur ausrechnen, 
sondern vielleicht auch noch schön formatiert ausgeben. 
Wir definieren uns hierzu eine Funktion pshow (kurz für "pretty show"):
-}
pshow :: Exp' -> String
pshow (ExpLeaf      n) = show n
pshow (ExpNode op l r) = concat ["(", pshow l, showOp op, pshow r, ")"]

showOp :: Op -> String
showOp Add = "+"
showOp Sub = "-"
showOp Mul = "*"
showOp Div = "/"

{-
Es fällt auf, dass die Funktionen eval' und pshow insofern gleichartig vorgehen, 
als dass für Blätter die jeweilige Beschriftung verarbeitet wird, 
während bei Knoten die Teilbäume zunächst rekursiv verarbeitet und 
dann mit der Knotenbeschriftung kombiniert werden. 
Dies entspricht genau dem Konzept einer Faltung auf Binärbäumen:
-}

foldTree :: (a -> c) -> (b -> c -> c -> c) -> ExpTree a b -> c
foldTree f _ (ExpLeaf     x) = f x
foldTree f g (ExpNode y l r) = g y (foldTree f g l) (foldTree f g r)

-- Somit können wir beide Funktionen auch mittels foldTree implementieren:
eval2 :: Exp' -> Int
eval2 = foldTree id sem

pshow2 :: Exp' -> String
pshow2 = foldTree show (\op s1 s2 -> concat ["(", s1, showOp op, s2, ")"])

-- ########## Suchbaum ##########
data SearchTree = EmptySearchTree | BranchST SearchTree Int SearchTree
  deriving (Eq, Show)

insert :: Int -> SearchTree -> SearchTree
insert x empty = BranchST EmptySearchTree x EmptySearchTree
insert x (BranchST lt n rt)
                            | x <= n     = (BranchST (insert x lt) n rt)
                            | otherwise  = (BranchST lt n (insert x rt))

isElem :: Int -> SearchTree -> Bool
isElem _ EmptySearchTree = False
isElem x (BranchST lt n rt) 
                          | x == n    = True
                          | x <= n    = isElem x lt
                          | otherwise = isElem x rt

delete :: Int -> SearchTree -> SearchTree
delete _ EmptySearchTree = EmptySearchTree
delete x t@(BranchST lt n rt) 
                           | isElem x t = if x == n then deleteAux t
                                          else if x < n then (BranchST (delete x t) n rt)
                                               else (BranchST lt n (delete x rt))
                           | otherwise = t

-- deletes the top node of a given tree
deleteAux :: SearchTree -> SearchTree
deleteAux (BranchST EmptySearchTree n rt) = rt
deleteAux (BranchST lt n EmptySearchTree) = lt
deleteAux (BranchST lt n rt) = (BranchST (delete n lt) (greatesLeft lt) rt)


greatesLeft :: SearchTree -> Int
greatesLeft (BranchST EmptySearchTree n _) = n
greatesLeft (BranchST lt _ _) = greatesLeft lt

testTree :: SearchTree
testTree = BranchST (BranchST (BranchST EmptySearchTree 1 EmptySearchTree) 2 (BranchST EmptySearchTree 3 EmptySearchTree))
                                4
                               (BranchST (BranchST EmptySearchTree 5 EmptySearchTree) 7 (BranchST EmptySearchTree 8 EmptySearchTree))


-- ########## Polymorphe binäre Blatt-Bäume  ##########
data PTree a = PLeaf a | PTree a :&: PTree a
  deriving (Eq,Show)

-- flatTree soll einen Baum von Bäumen zu einem Baum von Elementen flachklopfen
flatTree' :: PTree (PTree a) -> PTree a
flatTree' (PLeaf  x) = x
flatTree' (s :&: t) = flatTree s :&: flatTree t

-- bessere Lösung
flatTree :: PTree (PTree a) -> PTree a
flatTree = extendTree id

-- eine Funktion auf jedes Element anwendet
mapTree' :: (a -> b) -> PTree a -> PTree b
mapTree' f (PLeaf  x) = PLeaf (f x)
mapTree' f (s :&: t) = mapTree f s :&: mapTree f t

-- bessere Lösung
mapTree :: (a -> b) -> PTree a -> PTree b
mapTree f = extendTree (PLeaf . f)

--  einen Baum an den Blättern um neue Teilbäume erweitern.
extendTree' :: (a -> PTree b) -> PTree a -> PTree b
extendTree' f (PLeaf  x) = f x
extendTree' f (s :&: t) = extendTree' f s :&: extendTree' f t

-- bessere Lösung
extendTree :: (a -> PTree b) -> PTree a -> PTree b
extendTree f = foldTree' f (:&:)

-- andere Lösung
extendTree'' :: (a -> PTree b) -> PTree a -> PTree b
extendTree'' f = flatTree' . mapTree' f

-- soll einen Baum zu einem einzelnen Wert zusammenfalten
foldTree' :: (a -> b) -> (b -> b -> b) -> PTree a -> b
foldTree' l _ (PLeaf  x) = l x
foldTree' l f (s :&: t) = foldTree' l f s `f` foldTree' l f t

