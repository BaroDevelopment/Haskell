-- ############################## Cheat Sheet ##############################
{-
Int: -2^31 bis 2^31
Integer: begrenzt durch Speicher
not
div
mod
< > <= >=
Bool: True, False
/= ungleich bzw. XOR
Sowohl Typen als auch Konstruktoren müssen in Haskell mit Großbuchstaben beginnen!
: für Listen ist rechtsassoziativ
1:(2:(3:[])) = 1:2:3:[] = [1,2,3]
++ ist vordefiniert (append)
Opperationen kann man auch infix schreiben:    [1] ++ [2]   =   (++) [1] [2]
zweistellige Funktionen können infix geschrieben werden mit:   div 8 4  = 8 `div` 4
data MyType = ... deriving (Eq, Show, Ord)
type String = [Char]    d.h. "HALLO" = [H, A, L, L, O]
derive square 1.0   =   derive (\x -> x * x) 1.0    <-Lambda-Abstraktion bzw. Anonyme Funktion
-}

import Prelude hiding (min, lines)
import Data.Char

ggt :: Int -> Int -> Int
ggt x 0 = x
ggt x y = ggt y (x `mod` y)

kgv :: Int -> Int -> Int
kgv x y = (x * y) `div` ggt x y

ggtL :: [Int] -> Int
ggtL [] = 1
ggtL [x] = x
ggtL (x:xs) = ggt x (ggtL xs)

kgvL :: [Int] -> Int
kgvL [] = 1
kgvL [x] = x
kgvL (x:xs) = kgv x (kgvL xs)

factorial :: Integer -> Integer
factorial n = if n == 0 then 1
              else n * factorial (n - 1) 

factorial' n | n == 0     = 1
             | otherwise  = n * factorial' (n - 1)

            -- naiv implementation with complexity 2^n
fibonacci :: Integer -> Integer
fibonacci n = if n == 0 then 0
              else if n == 1 then 1
                    else fibonacci(n - 1) + fibonacci (n - 2)

            -- using Accumulator-Way   splitted in two methods
fibonacciA :: Integer -> Integer             
fibonacciA n = fibonacciA' 0 1 n

fibonacciA' :: Integer -> Integer -> Integer -> Integer
fibonacciA' fibn fibnp1 n = if n == 0 then fibn
                             else fibonacciA' fibnp1 (fibn + fibnp1) (n - 1)

            -- using Accumulator-Way   using where
fibonacciA2 :: Integer -> Integer
fibonacciA2 n = fibonacciA2' 0 1 n
                  where fibonacciA2' fibn fibnp1 n = 
                           if n == 0 then fibn
                           else fibonacciA2' fibnp1 (fibn + fibnp1) (n - 1)

            -- using Accumulator-Way   using let
fibonacciA3 :: Integer -> Integer
fibonacciA3 n = 
  let fibonacciA3' fibn fibnp1 n =
          if n == 0 then fibn
          else fibonacciA3' fibnp1 (fibn + fibnp1) (n - 1)
  in fibonacciA3' 0 1 n


fib2 :: Integer -> Integer
fib2 n = fib2' 0 1 n
  where
    fib2' :: Integer -> Integer -> Integer -> Integer
    fib2' fibn fibnp1 n =
      if n==0 
        then fibn
        else fib2' fibnp1 (fibn+fibnp1) (dec n)
    dec n = n - 1

isPrim :: Int -> Bool
isPrim n = n /= 1 && checkDiv (div n 2)
  where checkDiv :: Int -> Bool
        checkDiv m =
            m == 1 || mod n m /=0 && checkDiv (m - 1) -- && bindet Stärker als ||

-- Komposition von Funktionen
(.) :: (b -> c) -> (a -> b) -> a -> c 
(f . g) x = f (g x)

curry :: ((a, b) -> c) -> a -> b -> c 
curry f x y = f (x, y)

uncurry :: (a -> b -> c) -> (a, b) -> c 
uncurry f (x, y) = f x y

lines :: String -> [String]
lines "" = []
lines ('\n' : cs) = "" : lines cs
lines (c:cs)= case lines cs of
                 []       -> [[c]]
                 (l:ls)   -> (c:l) :ls

-- ############################## Higher Order Functions  ##############################
dx = 0.0001
derive :: (Float -> Float) -> (Float -> Float) 
derive f = f'
   where f' :: Float -> Float 
         f' x = (f (x + dx) - f x) / dx

-- bessere Lösung
derive' :: (Float -> Float) -> (Float -> Float) 
derive' f = \x -> (f (x + dx) - f x) / dx 

-- Drei Möglichkeiten wie man add Programmieren kann
add :: Int -> Int -> Int
add x y = x + y

add' :: Int -> Int -> Int
add'    = \x y -> x + y

add'' :: Int -> Int -> Int
add'' x = \y   -> x + y

flip :: (a -> b -> c) -> b -> a -> c
flip f = \ x y -> f y x

--(flip take) :: [a] -> Int -> [a] 
--(flip take) "Hello World!" :: Int -> [Char]



code :: Char -> Char 
code c 
   | c == 'Z' = 'A'
   | c == 'z' = 'a'
   | otherwise = chr (ord c + 1) 

code' :: Int -> String -> String
code' offSet = map (codeChar offSet)

codeStr :: String -> String 
codeStr "" = "" 
codeStr (c:cs) = code c : codeStr cs

codeChar :: Int -> Char -> Char
codeChar offSet c = chr ((ord c + offSet) `mod` 256)


checkSum :: [Char] -> Int
checkSum "" = 1
checkSum (c:cs) = (ord c + checkSum cs) `mod` 256 

checkSum' :: String -> Int
checkSum' = foldr (\c res -> (ord c + res) `mod` 256) 1


 











-- ############################## Funktionen als Datenstrukturen ##############################
type Array a = Int -> a

emptyArray :: Array a
emptyArray i = error ("Access to non-initialized component " ++ show i)

putIndex :: Array a -> Int -> a -> Array a
putIndex a i v = a'
   where a' j | i == j    = v
              | otherwise = a j

getIndex :: Array a -> Int -> a
getIndex a i = a i

-- ############################## Typklassen und Überladungen ##############################



data IntTree = Blank | Node IntTree Int IntTree

instance Eq IntTree where 
    Blank == Blank = True 
    Node tl n tr == Node tl' n' tr' = 
                                      tl == tl' && n == n' && tr == tr'
    _ == _ = False
    t1 /= t2 = not (t1 == t2)

-- data Ordering = LT | EQ | GT
{- 
class Eq a => Ord a where 
   compare :: a -> a -> Ordering
   (<), (<=), (>=), (>) :: a -> a -> Bool 
   max, min :: a -> a ->a
-}


{-
Aufgabe: Überprüfen Sie die Typen aller in der Vorlesung deﬁnierten Funktionen 
auf ihren allgemeinsten Typ! Diese lauten zum Beispiel: 
(+) :: Num a => [a] → [a] 
nub :: Eq a => [a] → [a] 
qsort :: Ord a => [a] → [a]
-}