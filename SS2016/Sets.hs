module Haskell.Sets where

type Set a = a -> Bool


empty :: Set a
empty = emptySet

emptySet :: Set a
--emptySet = \x -> False
--emptySet x = False
--emptySet _ = False
--emptySet = \_ -> False
emptySet = const False

whole :: Set a
whole = const True

insert :: Eq a => Set a -> a -> Set a
-- insert set x a | a == x || set a
insert set x = \y -> x == y|| set y

remove :: Eq a => Set a -> a -> Set a
remove set x = \a -> x /= a && set a

isElem :: a -> Set a -> Bool
isElem x set = flip ($) x set
--isElem = flip ($)

listToSet :: Eq a => [a] -> Set a
--listToSet (x:xs) = insert x (listToSet xs)
listToSet = foldr (flip insert ) emptySet

union :: Set a -> Set a -> Set a
--union s1 s2 = \a -> s1 a || s2 a
union = combine (||)

intersection :: Set a -> Set a -> Set a
--intersection s1 s2 = \a -> s1 a && s2 a
intersection = combine (&&)

combine ::(Bool -> Bool -> Bool) -> Set a -> Set a -> Set a
combine (*) s1 s2 = \a -> s1 a * s2 a

complement :: Set a -> Set a
--complement set = \a -> not (set a)
--complement = (not .)    --> Tricky
complement set = not . set

difference :: Set a -> Set a -> Set a
difference s1 s2 = s1 `intersection` complement s2

--let setA = listToSet [1,2,3]
--let setB = listToSet [4,2,5]
--2 `isElem` intersection setA setB
--3 `isElem` complement setA