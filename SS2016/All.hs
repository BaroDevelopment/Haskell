-- ###################
-- ##### Serie 5 #####
-- ###################

-- #### Aufgabe 3 - Suchbäume ###
-- 1. Geben Sie eine geeignete Datenstruktur an für einen Suchbaum vom Typ SearchTree, der Beschriftungen vom Typ Int enthält.

                        -- Erste Variante
data SearchTree = Empty
                | Node Int SearchTree SearchTree
  deriving (Eq, Show)

t1 :: SearchTree
t1 = Node 4 (Node 1 Empty (Node 3 Empty Empty)) (Node 9 (Node 5 Empty Empty) (Node 12 Empty Empty))

insert :: Int -> SearchTree -> SearchTree
insert x Empty = (Node x Empty Empty)
insert x (Node n l r)
                    | x == n = (Node n l r)
                    | x < n = (Node n (insert x l) r)
                    | otherwise = (Node n l (insert x r))

isElem :: Int -> SearchTree -> Bool
isElem _ Empty = False
isElem x (Node n l r)
                   | x == n = True
                   | x < n = isElem x l
                   | otherwise = isElem x r

delete :: Int -> SearchTree -> SearchTree
delete _ Empty = Empty
delete x (Node n Empty r)
         | x == n = (Node (getLowestElemFromRight r) Empty (delete (getLowestElemFromRight r) r))
         | otherwise = (Node n Empty (delete x r))
delete x (Node n l Empty) = delete x l
delete x (Node n l r)
         | x == n = (Node (getHighestElemFromLeft l) (delete (getHighestElemFromLeft l) l) r)
         | x < n = (Node n (delete x l) r)
         | otherwise = (Node n l (delete x r))

getHighestElemFromLeft :: SearchTree -> Int
getHighestElemFromLeft Empty =  0
getHighestElemFromLeft (Node n Empty Empty) = n
getHighestElemFromLeft (Node n l Empty) = n
getHighestElemFromLeft (Node n Empty r) =
            if r == Empty then n
            else getHighestElemFromLeft r
getHighestElemFromLeft (Node n l r) =
            if r == Empty then n
            else getHighestElemFromLeft r

getLowestElemFromRight :: SearchTree -> Int
getLowestElemFromRight Empty = 0
getLowestElemFromRight (Node n Empty Empty) = n
getLowestElemFromRight (Node n l Empty) =
            if l == Empty then n
            else getLowestElemFromRight l
getLowestElemFromRight (Node n l r) =
            if l == Empty then n
            else getLowestElemFromRight l
