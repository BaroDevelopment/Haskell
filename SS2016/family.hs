data Person = Christine | Heinz | Maria | Fritz
              | Monika | Herbert | Angelika | Hubert
              | Susanne | Norbert | Andreas
  deriving(Eq, Show)

ehemann :: Person -> Person
ehemann Christine = Heinz
ehemann Maria = Fritz
ehemann Monika = Herbert
ehemann Angelika = Hubert

-- Mutter-Kind-Beziehung :   mutter Kind = Mutter vom Kind
mutter :: Person -> Person
mutter Herbert = Christine
mutter Angelika = Christine
mutter Hubert = Maria
mutter Susanne = Monika
mutter Norbert = Monika
mutter Andreas = Angelika

vater :: Person -> Person
vater kind = ehemann (mutter kind)

-- großvater Enkel Großvater = Boolean
grossvater :: Person -> Person -> Bool
grossvater e g | g == vater (vater e ) = True
               | g == vater (mutter e) = True
               | otherwise             = False