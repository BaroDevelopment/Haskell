import Prelude hiding (min, (++), tail, head, last, concat, Maybe(..),
                       (!!), length, zip, unzip, lines, take)

double :: Int -> Int
double x = x+x

min :: Int -> Int -> Int
min x y = if x<y then x else y

fac :: Int -> Int
fac n | n == 0    = 1
      | otherwise = n * fac (n-1)

fib :: Integer -> Integer
fib n = if n==0 then 0
                else if n==1 then
                       1
                     else 
                       fib (n-1) + fib (n-2)

fib2 :: Integer -> Integer
fib2 n = fib2' 0 1 n
  where
    fib2' :: Integer -> Integer -> Integer -> Integer
    fib2' fibn fibnp1 n =
      if n==0 
        then fibn
        else fib2' fibnp1 (fibn+fibnp1) (dec n)
    dec n = n - 1                    

isPrim :: Int -> Bool
isPrim n = n/=1 && checkDiv (div n 2)
  where checkDiv :: Int -> Bool
        checkDiv m =
            m == 1 || mod n m /=0 && checkDiv (m - 1)

data Color = Red | Blue | Yellow

beautiful :: Color -> Bool
beautiful Yellow = True
beautiful _      = False

data Complex = Complex Float Float
  deriving Show

myComplexNumber :: Complex
myComplexNumber = Complex 3.0 4.0

addComplex :: Complex -> Complex -> Complex
addComplex (Complex r1 i1) (Complex r2 i2) = Complex (r1+r2) (i1+i2)

(++) :: [a] -> [a] -> [a]
[] ++ ys     = ys
(x:xs) ++ ys = x : (xs ++ ys)

fromTo :: Int -> Int -> [Int]
fromTo from to = if to<from then []
                            else from : (fromTo (from + 1) to)

data Tree a = Empty
            | Branch (Tree a) a (Tree a)
  deriving Show

-- converts a Tree to a List
treeToList :: Tree a -> [a]
treeToList Empty = []
treeToList (Branch tl x tr) = treeToList tl ++ [x] ++ treeToList tr

height :: Tree a -> Int
height Empty = 0
height (Branch tl _ tr) = 1 + max (height tl) (height tr)

data Maybe a = Nothing | Just a

isNothing :: Maybe a -> Bool
isNothing Nothing = True
isNothing (Just _) = False

length :: [a] -> Int
length [] = 0
length (_:xs) = 1+length xs

head :: [a] -> a
head (x:_) = x

tail :: [a] -> [a]
tail (_:xs) = xs

last :: [a] -> a
last [x] = x
last (_:xs@(_:_)) = last xs

concat :: [[a]] -> [a]
concat []       = []
concat (xs:xss) = xs ++ concat xss

(!!) :: [a] -> Int -> a
(x:_)  !! 0 = x
(_:xs) !! n = xs !! (n-1)

--type String = [Char]

--data Either a b = Left a | Right b

valOrLength :: Either Int String -> Int
valOrLength (Left n) = n
valOrLength (Right str) = length str

type BoolOrSomething a = Either Bool a

--data (,) a b = (,) a b
--data (,,) a b c = (,,) a b c

-- Besser nicht so definieren, sondern add :: Int -> Int -> Int 
add :: (Int,Int) -> Int
add (x,y) = x + y

--fst :: (a,b) -> a
--fst (x,_) = x

--snd :: (a,b) -> b
--snd (_,x) = x

zip :: [a] -> [b] -> [(a,b)]
zip []     _  = []
zip _      [] = []
zip (x:xs) (y:ys) = (x,y) : zip xs ys

unzip :: [(a,b)] -> ([a],[b])
unzip []          = ([],[])
unzip ((x,y):xys) = let (xs,ys) = unzip xys in
                      (x:xs,y:ys)

allJusts :: [Maybe a] -> [a]
allJusts [] = []
allJusts (mx:mxs) = case mx of
                      Nothing -> allJusts mxs
                      Just y  -> y:allJusts mxs

lines :: String -> [String]
lines ""        = []
lines ('\n':cs) = "":lines cs
lines (c:cs)    = case lines cs of
                    []         -> [[c]]
                    (str:strs) -> (c:str):strs

take :: Int -> [a] -> [a]
take 0 _      = []
take _ []     = []
take n (x:xs) = x : take (n-1) xs
















