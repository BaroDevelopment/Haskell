onkel(N, O) :- mutter(N, M), bruder(M, O).
onkel(N, O) :- vater(N, V), bruder(V, O).
vorfahre(N, V) :- eltern(N, V).
vorfahre(N, V) :- eltern(N, E), vorfahre(E, V).
eltern(K, E) :- mutter(K, E).
eltern(K, E) :- vater(K, E).
bruder(P, B) :- mutter(P, M), mutter(B, M), maennlich(B), B \= P.
member(E, L) :- append(_, [E], Xs), append(Xs, _, L).
lookup(K, KVS, V) :- member((K, V), KVS).

reverse([], []).
reverse([X|Xs], Zs) :- reverse(Xs, Ys), append(Ys, [X], Zs).