reversed :: [a] -> [a]
reversed [] = []
reversed (x : xs) = reversed xs ++ [x]

indexOf :: Int -> [Int] -> Maybe Int
indexOf _ [] = Nothing
indexOf e (x : xs) = indexOf' e (x:xs) 0
   where indexOf' _ [] _ = Nothing
         indexOf' e (x:xs) i =
          if e == x
          then Just i
          else indexOf' e xs (i+1)

perms :: [a] -> [[a]]
perms []     = [[]]
perms (x:xs) = concatMap (insert x) (perms xs)

insert :: a -> [a] -> [[a]]
insert x []     = [[x]]
insert e (x:xs) = (e:x:xs) : map (x:) (insert e xs)

inits :: [a] -> [[a]]
inits [] = [[]]
inits (x : xs) = [] : map (x :) (inits xs)

tails :: [a] -> [[a]]
tails [] = [[]]
tails (x : xs) = (x : xs) : tails xs

-- Lists for Tests
testList = 5:4:3:2:1:[]
testPerms = 1:2:3:[]