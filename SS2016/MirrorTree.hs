data Tree = Leaf Int | Node Int Tree Tree | Empty
        deriving(Eq, Show)

t1 = (Node 4 (Node 1 (Leaf 2) (Leaf 3)) (Node 6 (Leaf 5) Empty))

mirrorTree :: Tree -> Tree
mirrorTree Empty = Empty
mirrorTree (Leaf x) = (Leaf x)
mirrorTree (Node x leftTree rightTree) = Node x (mirrorTree rightTree) (mirrorTree leftTree)

