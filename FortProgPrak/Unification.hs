module Unification where

import Type
import Substitution
import HelperFunctions
import PrettyPrint

-- return the disagreement set of two given terms
-- t = t'  ==> ds(t, t') = {}
-- t or t' is variable and t /= t'   ==> ds(t, t') = {t, t'}
-- t = f(t1, ... tn) and t' = g(s1, ... , sm) with (n, m > 0)
--      if f /= g or m /= n  ==> ds(t,t') = {t,t'}
--      if f = g and m = n and ti = si for all i < k and tk /= sk ==> ds(t,t') = ds(tk, sk) 
ds :: Term -> Term -> Maybe (Term, Term)
ds (Var x) (Var y) | x == y = Nothing 
ds (Comb f ts1) (Comb g ts2)  | f == g && length ts1 == length ts2  = foldr mplus Nothing (zipWith ds ts1 ts2)                                                                                   
  where                                                                                -- für jeden Term wird ds berechnet
    mplus :: Maybe a -> Maybe a -> Maybe a
    mplus Nothing y = y
    mplus x       _ = x
ds t1 t2            = Just (t1, t2)


{-
zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
Prelude> let xs = [1..5]
Prelude> let ys = [6.. 10]
Prelude> zipWith (\x y -> x+y) xs ys 
[7,9,11,13,15]
-}

t1 = Var 0
t2 = Var 0
t3 = [Comb "h" [Var 0, Var 0], Comb "h" [Var 3, Var 5]]  -- q(A,A, A)
t4 = [Comb "h" [Var 3, Var 5], Var 4]  -- h(D, F)


-- Computation of most general unifier
{-
Eine Substitution sig heißt Unifikator für t1 und t2, falls
sig(t1) = sig(t2). t1 und t2 heißen dann unifizierbar.
sig heißt allgemeinster Unifikator (most general unifier, mgu), falls für alle Unifikatoren
sig' eine Substitution  existiert mit sig' = 0 ° sig , 
wobei die Komposition  0 ° sig  definiert ist durch  0 ° sig(t) = 0(sig(t)).
-}
unify :: Term -> Term -> Maybe Subst
unify t0 t1 = unifys t0 t1 (Subst [])
  where -- unifys versucht Substitution für t0 und t1 zu finden
    unifys :: Term -> Term -> Subst -> Maybe Subst
    unifys t0 t1 s = if equalTerms (apply s t0) (apply s t1) 
                        -- wendet subst auf beide Terme an und guckt ob sie gleich sind
                     then Just s  -- dann gibt es Substitution
                     else case ds (apply s t0) (apply s t1) of 
                             --berechnet das ds und prüft welcher Fall eintritt (siehe unten)
        Just ((Var i), t) -> if variableInTerm (Var i) t 
                             then Nothing
                             else unifys (apply s t0) (apply s t1) (compose (Subst [(i, t)]) s)
        Just (t, (Var i)) -> if variableInTerm (Var i) t 
                             then Nothing
                             else unifys (apply s t0) (apply s t1) (compose (Subst [(i, t)]) s)
        Nothing           -> Nothing
        Just (_, _)       -> Nothing