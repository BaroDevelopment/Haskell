module ExamplesAndTests where

import Type
import Substitution
import PrettyPrint
import Unification
import SLDTree

{- ################## EXAMPLE - DATA #################-}
termExample1 :: Term
termExample1 = Comb "q" [Comb "a" [], Comb "b" []]

termExample1Copy :: Term
termExample1Copy = Comb "q" [Comb "a" [], Comb "b" []]

testi = Comb "q" [Comb "a" [Comb "c" [Var 0]], Comb "b" [], Comb "f" [Comb "l" []]]

ruleExample1 :: Rule
ruleExample1 = Comb "q" [Comb "a" [], Comb "b" []] :- []

termExample2 :: Term -- p(A,A)
termExample2 = Comb "p" [Var 0, Var 0]

termExample3 :: Term -- p(A, D)
termExample3 = Comb "p" [Var 0, Var 3]

termExample4 :: Term -- A
termExample4 = Var 0

ruleExample2 :: Rule --p(A,A).
ruleExample2 = termExample2 :- []

ruleExample3 :: Rule
ruleExample3 = Comb "p" [Var 0, Var 1] :- [Comb "q" [Var 0, Var 2], Comb "p" [Var 2, Var 1]]

progExample1 :: Prog
progExample1 = Prog [ 
                      Comb "p" [Var 0, Var 1] :- [Comb "q" [Var 0, Var 2], Comb "p" [Var 2, Var 1]]
                      , Comb "p" [Var 0, Var 0] :- []
                      , Comb "q" [Comb "a" [], Comb "b" []] :- []
                    ]

goalExample1 :: Goal
goalExample1 = Goal [termExample1, termExample2]

substExample1 :: Subst  -- {D -> E}
substExample1 = Subst [(1, Var 4)]

substExample2 :: Subst -- {A -> B}
substExample2 = Subst [(0, Var 1)]

substExample3 :: Subst
-- { A -> f(B, g(A)), B -> C }
substExample3 = Subst [(0, Comb "f" [Var 1, Comb "g" [Var 0]]), (1, Var 2)]



sldRule0 :: Rule  -- p(X,Z) :- q(X,Y), p(Y,Z).
sldRule0 = Comb "p" [Var 23, Var 25] :- [Comb "q" [Var 23, Var 24], Comb "p" [Var 24, Var 25]]

sldRule1 :: Rule  --- p(X,X).
sldRule1 = Comb "p" [Var 23, Var 23] :- []

sldRule2 :: Rule  -- q(a,b).
sldRule2 = Comb "q" [Comb "a" [], Comb "b" []] :- []

sldProg :: Prog
sldProg = Prog [sldRule0, sldRule1, sldRule2]

sldGoal :: Goal
sldGoal = Goal [Comb "p" [Var 0, Comb "b" []]]

sldTree :: SLDTree
sldTree = sld sldProg sldGoal



{- ################## Test - DATA #################-}
testApply = pretty (apply substExample2 termExample3)
testCompose = pretty (apply (compose substExample2 substExample1) termExample3)