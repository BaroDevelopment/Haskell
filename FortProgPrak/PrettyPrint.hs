module PrettyPrint where

import Type
import Data.List (intercalate)

--Infinite list of variable names
varNames :: [String]
varNames = [ if n == 0 then [c] else c : show n
           | n <- [0 :: Integer ..], c <- ['A' .. 'Z']
           ]

class Pretty a where
  pretty :: a -> String

-- Instance of Pretty for Term
instance Pretty Term where
  pretty (Var     x) = varNames !! x
  pretty (Comb f []) = f
  pretty (Comb f ts) = f ++ '(' : intercalate "," (map pretty ts) ++ ")"

-- Instance of Pretty for Rule
instance Pretty Rule where
  pretty (t :- []) = pretty t ++ "."
  pretty (t :- (x:xs)) = pretty t ++ " :- " ++ prettyPrint (x :- xs)
     where 
        prettyPrint (t :- []) = pretty t ++ "."
        prettyPrint (t :- (x:xs)) = pretty t ++ ", " ++ prettyPrint (x :- xs)
  --pretty (t :- ts) = (pretty t) ++ " :-" ++ ' ' : intercalate ", " (map pretty ts) ++ "."


-- Instance of Pretty for Prog
instance Pretty Prog where
  pretty (Prog []) = ""
  pretty (Prog (x:xs)) = pretty x ++ "\n" ++  pretty (Prog xs)


--Instance of Pretty Goal
instance Pretty Goal where
  pretty (Goal []) = "?- ."
  pretty (Goal (t:[])) = "?- " ++ pretty t ++ "."
  pretty (Goal ts) = "?- " ++ intercalate ", " (map pretty ts) ++ "."


-- Instance of Pretty for Subst
instance Pretty Subst where
  pretty (Subst xs) = "{" ++ intercalate ", " [ pretty (Var (fst x)) ++ " -> " ++ pretty (snd x) | x <- xs ] ++ "}"

-- Instance of Pretty for SLDTree
instance Pretty SLDTree where
  pretty t = prettyTree 0 t
    where
      prettyTree :: Int -> SLDTree -> String
      prettyTree 0 (SLDTree g tuples) = pretty g ++ "\n" ++ concat [ showIntendedTuple 1 tuple | tuple <- tuples]
      showIntendedTuple :: Int -> (Subst, SLDTree) -> String
      showIntendedTuple 10 _                = ".\n.\n.\n"
      showIntendedTuple n (s, SLDTree g xs) = showIntended n g s ++ (concat [(showIntendedTuple (n+1) x) | x <- xs])
      showIntended n g s    = concat (replicate (n - 1) "|   ") ++ "+-- " ++ pretty s ++ "\n"  ++ concat (replicate (n) "|   ") ++ pretty g ++ "\n"  