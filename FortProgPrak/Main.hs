import PrettyPrint
import Substitution
import Unification
import SLDTree
import Parser
import ExamplesAndTests
import Strategy
import HelperFunctions
import Type

-- other imports
import System.IO
import System.Exit
import Control.Monad
import Data.Char
import System.IO
import Data.Either
import Data.List
import Data.List.Split


main :: IO ()
main = do
        --putStrLn "Type in dataname: "
        --fn <- getLine
        --fnString <- (readFile fn)
        fnString <- (readFile "family.pl")
        let loadedProg = parseProg fnString
        let programm = rights [loadedProg]
        putStrLn $ pretty $ head programm
        goal (head programm)


goal programm = do
        ----- GOAL FROM USER ----------
        putStrLn "Type in your goal: "
        inputGoal <- getLine
        ----- Parse Goal and take rights of Either --------------
        let loadedGoal = parseGoal inputGoal
        let userGoal = rights [loadedGoal]
        ----- User have to pick a Strategy --------------
        putStrLn "Select your strategy: \t \t  a) dfs \t b) bfs"
        userStrategy <- getLine
        let strategy = selectStrategy userStrategy
        ------------- Pick all available results ----------
        let goal1 = concat $ map pretty (solve strategy programm (head userGoal))
        ---------- Split reults Beispiel: {A -> susanne} {A -> norbert}
        let goalList = split (startsWith "{") goal1
        ----- Use helperfunction
        if (empty goal1 || head goalList == "{}") then putStrLn "false"
        else printMoreResult goalList
        ------ Print out thr SLD-Tree
        putStrLn "Do you want to print out the SLD-Tree? \t \t a) yes \t b) no"
        printDecision <- getLine
        if printDecision == "yes" then do
              putStrLn  $ pretty (sld programm (head userGoal))
              main
        else do 
               main


-- helper function
printMoreResult [] =   putStrLn " NO more results available "
printMoreResult (x:xs) = do
  putStr  (show x)
  moreResultsInput <- getLine
  if moreResultsInput == ";" then do
    printMoreResult xs
  else putStrLn  "Keine weiteren Ergebnisse"


--checks if a list is empty 
empty :: [a] -> Bool
empty (x:_) = False
empty [] = True