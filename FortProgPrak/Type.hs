module Type where

-- Alias type for variables
type VarIndex = Int

-- Data type for terms
data Term = Var VarIndex | Comb String [Term]
    deriving (Eq, Show)

-- Data type for program rules
data Rule = Term :- [Term]

-- Data type for programs
data Prog = Prog [Rule]

-- Data type for goals
data Goal = Goal [Term]
	deriving Show

-- Data type for substitutions
-- [{VarIndex -> Term}]
data Subst = Subst [(VarIndex, Term)]
    deriving (Show, Eq)

 -- Data type for SLD-Trees
data SLDTree = SLDTree Goal [(Subst, SLDTree)]
	deriving Show
