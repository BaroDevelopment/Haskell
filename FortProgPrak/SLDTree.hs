module SLDTree where

import Data.List
import Type
import Substitution
import Unification
import Data.Maybe
import PrettyPrint
import HelperFunctions
import Strategy

-- generates the SLDTree for a given program and a given goal.
sld :: Prog -> Goal -> SLDTree
sld   p g@(Goal xs)   = sld' p g (maximum (makeListofVariables g) + 1)

sld' :: Prog -> Goal -> VarIndex -> SLDTree
sld'  _ (Goal [])   n = SLDTree (Goal []) []
sld'  p g@(Goal xs) n = let rules = reName (n + 2) (searchRules (head xs) p) -- creating list of rules
                  in makeTree rules p g (n + 2)
			        where makeTree rules p goal@(Goal ((Comb "not" xs):ys)) m = case sld' p (Goal xs) m of
											 (SLDTree (Goal xs) []) -> SLDTree (Goal ys) []
											 (SLDTree _ _)          -> SLDTree  goal []
			              makeTree rules p g    m                         = SLDTree g (concat (map (\f -> fabulousFunction f g) rules))
						  													-- concat [[1], [2], [3]] ==> [1,2,3]
																			-- getLeftSide (term :- [term]) ==> term
					    where fabulousFunction rule (Goal goals) = let subs = putSubstTogether [(unify (head goals) (getLeftSide rule))] 
									                   in tryToApply subs rule m xs p



-- renames the variables in a list of rules
reName :: Int -> [Rule] -> [Rule]
reName _ []           = []
reName n r@(rule:rules) = (reName' n rule) :(reName n rules) 
	where reName' :: Int -> Rule -> Rule
	      reName' n (a@(Comb s xs) :- ts) = let neededList = nub ((makeListofVariables (Goal (xs )))++makeListofVariables (Goal (ts)))
	                                            subs       = Subst (makeRNSub  (max n (maximum neededList) + 1) neededList)
						                              in  (apply subs a) :- (map (\x -> apply subs x) ts)


-- makes the substitution needed to rename a Term
makeRNSub :: Int -> [VarIndex] -> [(VarIndex, Term)]
makeRNSub _ []     =  []
makeRNSub n all@(x:xs) = if x < n then (x, (Var n)) : (makeRNSub ( n + 1) xs) else makeRNSub (n + 1) all


--[1,2,3,4,5,6,7]
-- n = 0
--[(1, Var 0), (2, Var 1), (3, Var 2)]



-- put substitutions together
putSubstTogether :: [Maybe Subst] -> Maybe Subst
putSubstTogether xs
        | allNothing xs = Nothing
	    | otherwise     = putSubstTogether' xs
		where putSubstTogether' []	                    = Just (Subst [])
		      putSubstTogether' (Nothing : xs)          = putSubstTogether' xs
		      putSubstTogether' ((Just (Subst x)) : xs) = let Just (Subst y) = putSubstTogether' xs 
                                          				 in Just (Subst (x ++ y))

-- gibt alle passenden Regeln zu einem gegebenen Term und einem Programm an
searchRules :: Term -> Prog -> [Rule]
searchRules (Comb s xs) (Prog []) = []
searchRules (Comb s xs) (Prog (ac@((Comb a bs):- cs):ys)) 
                      | a == s    = ac : searchRules (Comb s xs) (Prog ys)
                      | otherwise = searchRules (Comb s xs) (Prog ys)


-- makes a list of all variables in a goal
-- Goal [Var 4 rest egal] ==> [4]  --- unwichtig
makeListofVariables :: Goal -> [VarIndex]
makeListofVariables (Goal []) = []
makeListofVariables (Goal (x:xs))= case x of
				   (Var y) -> [y] 
				   (Comb str ys) -> (makeListHelp ys) ++ (makeListofVariables (Goal xs))
							where makeListHelp [] = []
							      makeListHelp ((Var z):zs) = [z] ++ (makeListHelp zs)
							      makeListHelp ((Comb str os):zs)= (makeListHelp os) ++ (makeListHelp zs)

testGoal = Goal [Comb "q" [Var 1, Var 2, Comb "h" [Var 4, Var 5]], Comb "q" [Var 1, Var 2, Comb "h" [Var 4, Var 5]]]

-- uses a given strategy to solve the goal from a given program
solve:: (SLDTree -> [Subst]) -> Prog -> Goal -> [Subst]
solve strategy p g = strategy (sld p g)














tryToApply :: Maybe Subst -> Rule -> VarIndex -> [Term] -> Prog -> [(Subst, SLDTree)]
tryToApply Nothing      rule m xs p = []
tryToApply (Just subst) rule m xs p = [(subst, sld' p (Goal((map (\a -> apply subst a) (getRightSide rule))
								++ (map(\a -> apply subst a) (tail xs)))) m)]